#!/bin/sh
echo $@
# If domain begins with fail, fail the challenge by not completing it.
curl -X POST 'http://boulder:8055/set-txt' -d \
    "{\"host\": \"$2\", \
     \"value\": \"$3\"}"
# if [[ "$CERTBOT_DOMAIN" != fail* ]]; then

# fi
echo "$3" >> /etc/challenges/$2