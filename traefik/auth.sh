#!/bin/sh
echo $@
wget 'http://boulder:8055/set-txt' --spider --server-response --post-data "{\"host\": \"$2\",\"value\": \"$3\"}"
mkdir -p /etc/challenges/$2
echo "$3" > /etc/challenges/$2/index.html
mkdir -p /etc/dns/
echo "dns-rr=$2,16,`echo $3 | hexdump -vC |  awk 'BEGIN {IFS="\t"} {$1=""; print }' | awk '{sub(/\|.*/,"")}1'  | tr -d '\n' | tr -d ' '`" | rev | cut -c 3- | rev > /etc/dns/$2
