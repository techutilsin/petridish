#/bin/bash

# If domain begins with fail, fail the challenge by not completing it.# !/bin/bash
if [[ "$CERTBOT_DOMAIN" != fail* ]]; then
    wget --spider  --server-response 'http://boulder:8055/set-txt' --post-data \
        "{\"host\": \"_acme-challenge.$CERTBOT_DOMAIN.\", \
         \"value\": \"$CERTBOT_VALIDATION\"}"
fi
