# ![PetriDish](PetriDish.png)

[PetriDish](https://en.wikipedia.org/wiki/Petri_dish) is are small circular glass dishes used in microbiology to provide the organisms the a suitable environment for culture cells experiments to be performed.

In case of software, it is similar in nature. It is the place where you go to test your culture cells, that is your software. Instead of creating a mock mode inside your code and then testing, this application frame allows you to test your application without letting it know that it is inside a safe zone.

This tool aims to provide an end-to-end mock environment instead, where tests can be performed, automation suites can be performed.

## Modules

### Done

- [API Mock Server](http://mock-server.com/)
- [DNSMASQ](https://github.com/jpillora/docker-dnsmasq)
- [Local AWS](https://localstack.cloud/)
- [Traefik: Reverse Proxy](https://traefik.io/)
- [Local Certificate Authority: Let's Encrypt Boulder](https://github.com/letsencrypt/boulder)
- [Local Certificate Generator: CertBot](https://certbot.eff.org/)
- [Tracing: Zipkin](https://zipkin.io/)
- [Tracing: Grafana](https://grafana.com/)
- [Tracing: Prometheus](https://prometheus.io/)
- [Docker Monitor: Portainer](https://www.portainer.io/)
- Demo Services:
  - AWS CLI
  - Mongo DB
  - whoami.com

### Partially implemented

- [Time Traveller](https://github.com/wolfcw/libfaketime)

### Narrowed down but yet to implement

- [Email Server](https://mailu.io/)
- [LDAP](https://www.openldap.org/)

### Dropped as of now

- ~~[MITM Proxy](https://mitmproxy.org/)~~

### To Be Decided

- Snapshoting

## Birds Eye View

![System](petridish-system.png)
