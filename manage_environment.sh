#! /usr/bin/env bash

# Initialize variables
# readonly traefik_url="traefik.localhost.com"
readonly basedir=$(dirname $0)
# readonly doc_file=$basedir"/docker-compose.yml"
doc_file=$basedir"/docker-compose.yml"

# Stop and remove Docker environment
down_environment() {
    echo "Stopping Docker environment"
    ! docker-compose -f $doc_file down -v &>/dev/null && \
        echo "[ERROR] Unable to stop the Docker environment" && exit 11
}

# Create and start Docker-compose environment or subpart of its services (if services are listed)
# $@ : List of services to start (optional)
up_environment() {
    echo "Starting Docker environment $@"
    ! docker-compose -f $doc_file up -d $@ &>/dev/null && \
        echo "[ERROR] Unable to start Docker environment $@" && exit 21
}

restart_service() {
    echo "Restarting Service: $@"
    ! docker-compose -f $doc_file restart $@ &>/dev/null && \
        echo "[ERROR] Unable to restart service $@" && exit 21
}

init_environment() {
    echo "Cleaning Out Old Environment Data" && \
    rm -rf $basedir/traefik/acme.json ; \
    rm -rf $basedir/acme/*  ; \
    # rm -rf $basedir/certs/*  ; \
    rm -rf $basedir/dnsmasq/confs/* ; \
    rm -rf $basedir/logs/* ; \
    rm -rf $basedir/nginx/challenges/* ; \
    rm -rf $basedir/localstack/* ; \
    touch  $basedir/traefik/acme.json ; \
    chmod 600 $basedir/traefik/acme.json # Needed for ACME
}

# Start all the environement
start_boulder() {
    echo "Start boulder environment"
    up_environment bmysql bhsm boulder
    waiting_counter=12
    echo "WAIT for boulder..."
    while [[ -z $(curl -s http://127.0.0.1:4000/directory) ]]; do
        echo "waiting for boulder..."
        sleep 5
        let waiting_counter-=1
        if [[ $waiting_counter -eq 0 ]]; then
            echo "[ERROR] Unable to start boulder container in the allowed time, the Docker environment will be stopped"
            down_environment
            exit 41
        fi
    done
}

# Script usage
show_usage() {
    echo
    echo "USAGE : manage_environment.sh --certs   # generate certificates"
    echo "                              --clean   # clean out all meta data"
    echo "                              --start   # start support and services containers"
    echo "                              --stop    # stop the environment"
    echo "                              --restart # restart the support and services containers"
    echo
}

execute_certbot(){
    docker-compose run certbot certonly -d $1 -n --preferred-challenges dns --manual-auth-hook /etc/scripts/manual-dns-auth.sh --agree-tos -m admin@techutils.in --manual --manual-public-ip-logging-ok --server http://traefik.boulder.com:4001/directory
}

generate_environment_certificates() {
    
    echo "Starting Local DNS Server"
    init_environment
    up_environment dns
    up_environment whoami
    echo "Starting Local Certificate Authority"
    start_boulder
    up_environment dnsChallenger
    up_environment traefik
    sleep 30s
    execute_certbot amazonaws.com
    execute_certbot queue.amazonaws.com,eu-west-1.queue.amazonaws.com
    execute_certbot eu-west-1.amazonaws.com,sqs.eu-west-1.amazonaws.com,sns.eu-west-1.amazonaws.com
    execute_certbot whoami.com
    sleep 15s
    restart_service dns
    restart_service traefik
    find ./acme/archive/ -name "*.pem" -exec ./changeName.sh {} ./certs \;
    down_environment
    echo "Generated Certificates: Certificates found in ./acme/archive/*/"
}

start_support_containers(){

    echo "Starting Support containers"
    up_environment dns
    # up_environment whoami
    up_environment awsstack
    # up_environment zipkin
    # up_environment dependencies
    # up_environment grafana
    # up_environment setup_grafana_datasource
    up_environment traefik
    # up_environment mockServer
    up_environment portainer
}


# Script usage
start_all_services() {
    echo "Starting Services containers"
    # up_environment awscli
    # up_environment mongodb
    # up_environment client
    up_environment pytest
}

# Main method
# $@ All parameters given
main() {

    [[ $# -ne 1 ]] && show_usage && exit 1

    case $1 in
        "--certs")
            doc_file=$basedir"/certificates.yml"
            generate_environment_certificates
            ;;
        "--clean")
            init_environment
            ;;
        "--start")
            doc_file=$basedir"/support.yml -f "$basedir"/services.yml"
            start_support_containers
            start_all_services
            ;;
        "--stop")
            doc_file=$basedir"/support.yml -f "$basedir"/services.yml"
            ! down_environment
            echo "ENVIRONMENT SUCCESSFULLY STOPPED"
            ;;
        "--restart")
            doc_file=$basedir"/support.yml -f "$basedir"/services.yml"
            down_environment
            start_support_containers
            start_all_services
            ;;
        *)
            show_usage && exit 2
            ;;
    esac
}

main $@